--
-- File generated with SQLiteStudio v3.2.1 on Rab Feb 20 23:54:10 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: edge
CREATE TABLE edge (id VARCHAR PRIMARY KEY, source INTEGER, target INTEGER, length INTEGER);
INSERT INTO edge (id, source, target, length) VALUES ('e1', 0, 1, 4);
INSERT INTO edge (id, source, target, length) VALUES ('e2', 0, 2, 3);
INSERT INTO edge (id, source, target, length) VALUES ('e3', 1, 2, 1);
INSERT INTO edge (id, source, target, length) VALUES ('e4', 1, 3, 2);
INSERT INTO edge (id, source, target, length) VALUES ('e5', 2, 3, 4);
INSERT INTO edge (id, source, target, length) VALUES ('e6', 3, 4, 2);
INSERT INTO edge (id, source, target, length) VALUES ('e7', 4, 5, 6);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
