package stu.upnvj.damkargo;

public class JarakHelper {
    private String namaPos;
    private String jarakPos;

    public JarakHelper(String namaPos, String jarakPos) {
        this.namaPos = namaPos;
        this.jarakPos = jarakPos;
    }

    public String getNamaPos() {
        return namaPos;
    }

    public String getJarakPos() {
        return jarakPos;
    }

    public void setNamaPos(String namaPos) {
        this.namaPos = namaPos;
    }

    public void setJarakPos(String jarakPos) {
        this.jarakPos = jarakPos;
    }
}
