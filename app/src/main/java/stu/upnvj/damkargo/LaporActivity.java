package stu.upnvj.damkargo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LaporActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker myMarker;

    private FirebaseDatabase database;
    private DatabaseReference getReference;

    private BottomSheetBehavior mBottomSheetBehaviour;

    private Toolbar toolbar;

    private FloatingActionButton fab;
    private TextView toolbar_text;
    private EditText alamat;
    private ArrayList<AlamatHelper> mArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AlamatAdapter mAdapter;

    private KoorCallback koorCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_view);

        //Firebase inisialisasi
        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();
        getReference = database.getReference().child("pos damkar");

        // Initialize the AutocompleteSupportFragment.
        Places.initialize(getApplicationContext(), getString(R.string.google_api_key));

        //Toolbar
        toolbar = findViewById(R.id.toolbar2);

        TextView toolbarText = findViewById(R.id.toolbar_text);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText("Lapor Lokasi Kebakaran");
            setSupportActionBar(toolbar);
        }

        alamat = findViewById(R.id.alamat);
        recyclerView = findViewById(R.id.recView);

        placeAutoComplete();
        getCoor();
        bottomSheet();

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator( new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_lapor);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        addMarker();
        boolean permissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (permissionGranted) {

            googleMap.setMyLocationEnabled(true);
            final LatLng jakarta = new LatLng(-6.177181, 106.827070);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(jakarta, 10));
            myMarker = mMap.addMarker(new MarkerOptions()
                    .position(jakarta)
                    .title("Lokasi Kebakaran")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            koorCallback = new KoorCallback() {
                @Override
                public void onCallback(LatLng koor) {

                    myMarker.setPosition(koor);
                    fabListener(koor);

                }
            };

            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick (LatLng latLng){
                    if (myMarker == null) {

                        // Marker was not set yet. Add marker:
                        myMarker = mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title("Lokasi Kebakaran")
                                .snippet("Lokasi Kebakaran")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                        LatLng tkp = myMarker.getPosition();
                        fabListener(tkp);
                    } else {

                        // Marker already exists, just update it's position
                        myMarker.setPosition(latLng);
                        LatLng tkp = myMarker.getPosition();
                        fabListener(tkp);
                    }
                }
            });

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }
    }

    public LatLng fabListener(LatLng tkp) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaporActivity.this, JarakTerdekat.class);
                intent.putExtra("Titik Koordinat", tkp);
                startActivity(intent);
            }
        });
        return tkp;
    }

    public void bottomSheet() {

        fab = findViewById(R.id.fab);
        // get the bottom sheet view
        View nestedScrollView = findViewById(R.id.nestedScrollView2);
        mBottomSheetBehaviour = BottomSheetBehavior.from(nestedScrollView);

        // change the state of the bottom sheet
        mBottomSheetBehaviour.setPeekHeight(225);
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);

        // set callback for changes
        mBottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                String state = "";

                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING: {
                        fab.animate().scaleX(0).scaleY(0).setDuration(300).start();
                        break;
                    }
                    case BottomSheetBehavior.STATE_SETTLING: {
                        state = "SETTLING";
                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        state = "EXPANDED";
                        break;
                    }
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        fab.animate().scaleX(1).scaleY(1).setDuration(300).start();
                        break;
                    }
                    case BottomSheetBehavior.STATE_HIDDEN: {
                        state = "HIDDEN";
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                fab.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0).start();
            }
        });
    }

    public void placeAutoComplete() {
        final PlacesClient placesClient = Places.createClient(this);

        alamat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                clearData();
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void afterTextChanged(Editable s) {
                RectangularBounds jakarta_bound = RectangularBounds.newInstance(
                        new LatLng(-6.419025, 106.616821),
                        new LatLng(-6.023117, 106.984863));
                String query = s.toString();
                AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

                Log.i("Places", query);
                FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                        // Call either setLocationBias() OR setLocationRestriction().
                        .setLocationBias(jakarta_bound)
                        .setCountry("id")
                        .setTypeFilter(TypeFilter.ADDRESS)
                        .setSessionToken(token)
                        .setQuery(query)
                        .build();

                placesClient.findAutocompletePredictions(request).addOnSuccessListener(new OnSuccessListener<FindAutocompletePredictionsResponse>() {
                    AlamatHelper alamatHelper = null;
                    @Override
                    public void onSuccess(FindAutocompletePredictionsResponse response) {
                        for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                            alamatHelper = new AlamatHelper(prediction.getPrimaryText(null).toString(), prediction.getSecondaryText(null).toString(), prediction.getPlaceId());
                            mArrayList.add(alamatHelper);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (exception instanceof ApiException) {
                            ApiException apiException = (ApiException) exception;
                            Log.e("Places", "Place not found: " + apiException.getStatusCode());
                        }
                    }
                });
            }
        });

    }

    public void getCoor() {
        final PlacesClient placesClient = Places.createClient(this);
        mAdapter = new AlamatAdapter(mArrayList, (position, id) -> {
            AlamatHelper alamatHelper = mArrayList.get(position);

            alamat.setText(alamatHelper.getPrimary());

            // Define a Place ID.
            String placeId = alamatHelper.getIdAlamat();

            // Specify the fields to return (in this example all fields are returned).
            List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG);

            // Construct a request object, passing the place ID and fields array.
            FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();

            placesClient.fetchPlace(request).addOnSuccessListener(response -> {
                Place place = response.getPlace();
                final LatLng marker = place.getLatLng();
                Log.i("Place", "Place found: " + place.getLatLng().toString());
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                koorCallback.onCallback(marker);

            }).addOnFailureListener(exception -> {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    int statusCode = apiException.getStatusCode();
                    // Handle error with given status code.
                    Log.e("Place", "Place not found: " + exception.getMessage());
                }
            });
        });
    }

    public void clearData() {
        mArrayList.clear(); // clear list
        mAdapter.notifyDataSetChanged(); // let your adapter know about the changes and reload view.
        recyclerView.removeAllViews();
    }

    public void addMarker() {
        getReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){

                    PosHelper pos = child.getValue(PosHelper.class);
                    LatLng location = new LatLng(pos.getLatitude(),pos.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(location).title(pos.getNama()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private interface KoorCallback {
        void onCallback (LatLng koor);
    }

    private void log(String str) {
        Log.i("GH", str);
    }

    private void log(String str, Throwable t) {
        Log.i("GH", str, t);
    }

    private void logUser(String str) {
        log(str);
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }


}
