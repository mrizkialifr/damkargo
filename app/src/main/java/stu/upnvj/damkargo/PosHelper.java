package stu.upnvj.damkargo;

public class PosHelper {

    public String nama;
    public Double latitude;
    public Double longitude;


    public String getNama() {
        return nama;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public PosHelper(){

    }

    public PosHelper(String nama, Double latitude, Double longitude) {
        this.nama = nama;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
