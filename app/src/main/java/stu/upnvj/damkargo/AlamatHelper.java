package stu.upnvj.damkargo;

public class AlamatHelper {

    private String primary;
    private String secondary;
    private String idAlamat;


    public AlamatHelper(String primary, String secondary, String idAlamat) {
        this.primary = primary;
        this.secondary = secondary;
        this.idAlamat = idAlamat;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getSecondary() {
        return secondary;
    }

    public void setSecondary(String secondary) {
        this.secondary = secondary;
    }

    public String getIdAlamat() {
        return idAlamat;
    }

    public void setIdAlamat(String idAlamat) {
        this.idAlamat = idAlamat;
    }
}
