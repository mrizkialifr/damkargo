package stu.upnvj.damkargo;

import com.google.android.gms.maps.model.LatLng;

public class Haversine {
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    public static double distance2(double startLat, double startLong,
                                  double endLat, double endLong) {


        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c; // <-- d
    }

    public static double distance(LatLng start, LatLng end) {

        double startLatDec = start.latitude;
        double startLongDec = start.longitude;
        double endLatDec = end.latitude;
        double endLongDec = end.longitude;

        double startLat = Math.toRadians(startLatDec);
        double endLat   = Math.toRadians(endLatDec);

        double dLat  = Math.toRadians((endLatDec - startLatDec));
        double dLong = Math.toRadians((endLongDec - startLongDec));

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c; // <-- d
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}
