package stu.upnvj.damkargo;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Path;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.util.Constants;
import com.graphhopper.util.Parameters;
import com.graphhopper.util.PointList;
import com.graphhopper.util.StopWatch;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RuteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private UiSettings mUiSettings;
    private Marker myMarker;
    private Circle myCircle;
    private Polyline rutePoly;

    private List<Marker> markers = new ArrayList<>();
    private List<LatLng> hidranMarker = new ArrayList<>();
    private HashMap<LatLng, Double> hidranList = new HashMap<>();

    private Boolean buttonHidran = false;

    private Toolbar toolbar;

    private TextView jarak, waktu;
    private Button hidran;

    private BottomSheetBehavior mBottomSheetBehaviour;

    private Dialog dialog;

    private File mapsFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "/damkargo/maps/");
    private String currentArea = "jakarta";
    private GraphHopper hopper;
    private MyGraphhopper hoppers;
    private Callback callback;
    private FirebaseDatabase database;
    private DatabaseReference getReferencePos, getReferenceHidran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rute);

        toolbar = findViewById(R.id.toolbar2);
        jarak = findViewById(R.id.jarak_txt);
        hidran = findViewById(R.id.hidran_txt);

        TextView toolbarText = findViewById(R.id.toolbar_text);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText("Rute Terpendek");
            setSupportActionBar(toolbar);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bottomSheet();
        loadGraphStorage();

        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();
        getReferencePos = database.getReference().child("pos damkar");
        getReferenceHidran = database.getReference().child("hidran");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        addMarker();

        LatLng tkp = getIntent().getExtras().getParcelable("Titik tkp");
        LatLng pos = getIntent().getExtras().getParcelable("Titik pos");

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
        mMap.addMarker(new MarkerOptions()
                .position(tkp)
                .title("Lokasi Kebakaran")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));


        calcPath(pos, tkp);
        addHidran(tkp);
        hidran.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!buttonHidran){
                    hidran.setText("Sembunyikan Hidran");
                    int i = 0;

                    do {
                        log("jarak ke : " + i);
                        for (LatLng loc: hidranList.keySet()) {
                            if (hidranList.get(loc) < i) {
                                hidranMarker.add(loc);
                            }
                            log("key : " + loc);
                            log("value : " + hidranList.get(loc));
                        }
                        i++;
                    } while (hidranMarker.isEmpty());
                    log("jarak radius : " + i);
                    myCircle = mMap.addCircle(new CircleOptions()
                            .center(tkp)
                            .radius((i-1)*1000)
                            .strokeWidth(2f)
                            .strokeColor(Color.rgb(0, 136, 255))
                            .fillColor(Color.argb(20, 0, 136, 255)));

                    for (LatLng hidranLoc : hidranMarker){
                        myMarker = mMap.addMarker(new MarkerOptions()
                                .position(hidranLoc)
                                .title("Hidran")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        markers.add(myMarker);
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tkp, 14));
                    Log.i("radius ", i + " km");
/*                    myCircle.remove();
                    myMarker.remove();*/
                    buttonHidran = true;
                } else {
                    hidran.setText("Tampilkan Hidran");
                    for (int x = 0; x < markers.size(); x++) {
                        markers.get(x).setVisible(false);
                    }
                    myCircle.remove();
                    hidranMarker.clear();
                    buttonHidran = false;
                }

                /*if (markers == null) {
                    Toast.makeText(RuteActivity.this, "Hydrant not found!", Toast.LENGTH_SHORT).show();
                    hidran.setVisibility(View.INVISIBLE);
                }
                else {
                    if (!myCircle.isVisible() && !myMarker.isVisible()){
                        for (int i = 0; i < markers.size(); i++) {
                            markers.get(i).setVisible(true);
                        }
                        myCircle.setVisible(true);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tkp, 14));
                        hidran.setText("Hide Hydrant");
                    }
                    else {
                        for (int i = 0; i < markers.size(); i++) {
                            markers.get(i).setVisible(false);
                        }
                        myCircle.setVisible(false);
                        hidran.setText("Show Hydrant");
                    }
                }*/

            }
        });

    }

    public void bottomSheet() {

        // get the bottom sheet view
        View nestedScrollView = findViewById(R.id.nestedScrollView2);
        mBottomSheetBehaviour = BottomSheetBehavior.from(nestedScrollView);

        // change the state of the bottom sheet
        mBottomSheetBehaviour.setPeekHeight(260);
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);

        // set callback for changes
        mBottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                String state = "";

                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING: {
                        break;
                    }
                    case BottomSheetBehavior.STATE_SETTLING: {
                        state = "SETTLING";
                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        state = "EXPANDED";
                        break;
                    }
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        break;
                    }
                    case BottomSheetBehavior.STATE_HIDDEN: {
                        state = "HIDDEN";
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    private void addHidran(LatLng lokasi) {
        int i = 1;


        getReferenceHidran.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {

                    HidranHelper hidranHelper = child.getValue(HidranHelper.class);
                    Double lat = Double.parseDouble(hidranHelper.getLatitude());
                    Double lng = Double.parseDouble(hidranHelper.getLongitude());
                    LatLng location = new LatLng(lat, lng);

                    Double jarak = Haversine.distance(location, lokasi);
                    log("Cobaba" + String.valueOf(jarak));
                    hidranList.put(location, jarak);
                    /*
                    if (jarak <= i) {
                        log("Cobaba di if" + String.valueOf(jarak));
                    myMarker = mMap.addMarker(new MarkerOptions()
                            .position(location)
                            .title(hidranHelper.getAlamat())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                    myMarker.setVisible(false);
                    markers.add(myMarker);
                    log("Alamat Hidran : " + hidranHelper.getAlamat() + " Jarak : " + jarak.toString());
                    }
                    else if (jarak <=2 && jarak >= 1) {
                        myMarker = mMap.addMarker(new MarkerOptions()
                                .position(location)
                                .title(hidranHelper.getAlamat())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        myMarker.setVisible(false);
                        markers.add(myMarker);
                    }*/
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


/*        myCircle = mMap.addCircle(new CircleOptions()
                .center(lokasi)
                .radius(1000)
                .strokeWidth(2f)
                .strokeColor(Color.rgb(0, 136, 255))
                .fillColor(Color.argb(20, 0, 136, 255)));
        myCircle.setVisible(false);*/
    }

    public void addMarker() {
        getReferencePos.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){

                    PosHelper pos = child.getValue(PosHelper.class);

                    LatLng location = new LatLng(pos.getLatitude(),pos.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(location).title(pos.getNama()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void loadGraphStorage() {
        logUser("loading graph (" + Constants.VERSION + ") ... ");
        new GHAsyncTask<Void, Void, Path>() {
            protected Path saveDoInBackground(Void... v) throws Exception {
                GraphHopper tmpHopp = new GraphHopper().forMobile().setCHEnabled(false);
                tmpHopp.load(new File(mapsFolder, currentArea).getAbsolutePath() + "-gh");
                log("found graph " + tmpHopp.getGraphHopperStorage().toString() + ", nodes:" + tmpHopp.getGraphHopperStorage().getNodes());
                hopper = tmpHopp;
                return null;
            }


            protected void onPostExecute(Path o) {
                if (hasError()) {
                    logUser("An error happened while creating graph:"
                            + getErrorMessage());
                } else {
                    logUser("Finished loading graph.");
                }
            }
        }.execute();
    }

    public void calcPath(final LatLng from, final LatLng to) {

        final double fromLat = from.latitude;
        final double fromLon = from.longitude;
        final double toLat = to.latitude;
        final double toLon = to.longitude;


        log("calculating path ...");
        new AsyncTask<Void, Void, PathWrapper>() {
            float time;

            protected PathWrapper doInBackground(Void... v) {
                StopWatch sw = new StopWatch().start();
                GHRequest req = new GHRequest(fromLat, fromLon, toLat, toLon)
                        .setAlgorithm(Parameters.Algorithms.ASTAR)
                        /*.setWeighting("shortest")*/;
                req.getHints()
                        .put(Parameters.Routing.INSTRUCTIONS, "false");
                GHResponse resp = hopper.route(req);
                time = sw.stop().getSeconds();
                return resp.getBest();
            }

            protected void onPostExecute(PathWrapper resp) {
                if (!resp.hasErrors()) {
                    log("from:" + fromLat + "," + fromLon + " to:" + toLat + ","
                            + toLon + " found path with distance:" + resp.getDistance()
                            / 1000f + ", nodes:" + resp.getPoints().getSize() + ", time:"
                            + time + " " + resp.getDebugInfo());
                    logUser("rute ditemukan dengan jarak " + (int) (resp.getDistance() / 100) / 10f
                            + "km, waktu:" + resp.getTime() / 60000f + "min, debug:" + time);
                    DecimalFormat df = new DecimalFormat("#.##");
                    jarak.setText(df.format((resp.getDistance() / 100) / 10f) + " km");
                    createPathLayer(resp);
                } else {
                    logUser("Error:" + resp.getErrors());
                }
            }
        }.execute();
    }

    private void createPathLayer(PathWrapper response) {

        List<LatLng> geoPoints = new ArrayList<>();
        PointList pointList = response.getPoints();
        for (int i = 0; i < pointList.getSize(); i++) {
            geoPoints.add(new LatLng(pointList.getLatitude(i), pointList.getLongitude(i)));
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < geoPoints.size(); i++) {
            Double lat = geoPoints.get(i).latitude;
            Double lng = geoPoints.get(i).longitude;
            LatLng coor = new LatLng(lat, lng);
            builder.include(coor);
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 12));


        rutePoly = mMap.addPolyline(new PolylineOptions()
                .addAll(geoPoints)
                .width(15)
                .color(Color.BLUE)
                .geodesic(true));

        /*callback.onCallback(builder.build());*/
    }

    private interface Callback {
        void onCallback (LatLngBounds latLngBounds);
    }

    private void log(String str) {
        Log.i("GH", str);
    }

    private void log(String str, Throwable t) {
        Log.i("GH", str, t);
    }

    private void logUser(String str) {
        log(str);
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }


}

