package stu.upnvj.damkargo;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JarakTerdekat extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<JarakHelper> mArrayList = new ArrayList<>();
    private HashMap<String, Double> hashMap;
    private JarakAdapter mAdapter;

    private Toolbar toolbar;

    private FirebaseDatabase database;
    private DatabaseReference getReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jarak_terdekat);

        toolbar = findViewById(R.id.toolbar2);

        TextView toolbarText = findViewById(R.id.toolbar_text);
        if(toolbarText!=null && toolbar!=null) {
            toolbarText.setText("Jarak Pos Pemadam Kebakaran");
            setSupportActionBar(toolbar);
        }

        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new JarakAdapter(mArrayList, new OnRecyclerClickListner() {
            @Override
            public void onRecyclerViewItemClicked(int position, int id) {
                JarakHelper jarak = mArrayList.get(position);
                readData(new FirebaseCallback() {
                    @Override
                    public void onCallback(LatLng pos) {
                        Log.i("Callback", pos.toString());
                        LatLng tkp = getIntent().getExtras().getParcelable("Titik Koordinat");
                        Intent intent = new Intent(JarakTerdekat.this, RuteActivity.class);
                        intent.putExtra("Titik tkp", tkp);
                        intent.putExtra("Titik pos", pos);
                        startActivity(intent);
                    }
                }, jarak.getNamaPos());
            }
        });

        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();
        getReference = database.getReference().child("pos damkar");

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator( new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        LatLng posisi = getIntent().getExtras().getParcelable("Titik Koordinat");
        calcDist(posisi);
    }

    private interface FirebaseCallback {
        void onCallback(LatLng koor);
    }

    private void readData(final FirebaseCallback firebaseCallback, String pos) {
        Query query = getReference.orderByChild("nama").equalTo(pos);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // dataSnapshot is the "issue" node with all children with id 0
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        PosHelper pos = child.getValue(PosHelper.class);
                        LatLng location = new LatLng(pos.getLatitude(),pos.getLongitude());
                        firebaseCallback.onCallback(location);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void hashToArray (HashMap<String, Double> hashMap1) {
        JarakHelper jarak = null;
        for (HashMap.Entry<String,Double> entry : hashMap1.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            String value1 = value.toString();
            jarak = new JarakHelper(key, value1);
            mArrayList.add(jarak);
        }
        mAdapter.notifyDataSetChanged();
    }

    public static HashMap<String, Double> sortByValue(HashMap<String, Double> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Double> > list =
                new LinkedList<Map.Entry<String, Double> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Double> >() {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Double> temp = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public void calcDist (final LatLng tkp) {
        hashMap = new HashMap<>();
        getReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    PosHelper pos = child.getValue(PosHelper.class);
                    LatLng lokasiPos = new LatLng(pos.getLatitude(), pos.getLongitude());
                    Double distance = Haversine.distance(tkp, lokasiPos);
                    distance = Math.floor(distance * 100) / 100;
                    hashMap.put(pos.getNama(), distance);
                }
                HashMap<String, Double> hashMap1 = sortByValue(hashMap);
                hashToArray(hashMap1);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}