package stu.upnvj.damkargo;

public class HidranHelper {

    public String alamat;
    public String latitude;
    public String longitude;

    public HidranHelper(String alamat, String latitude, String longitude) {
        this.alamat = alamat;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public HidranHelper () {

    }

    public String getAlamat() {
        return alamat;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
