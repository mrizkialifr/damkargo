package stu.upnvj.damkargo;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class JarakAdapter extends RecyclerView.Adapter<JarakAdapter.MyViewHolder> {
    private ArrayList<JarakHelper> arrayList;
    private HashMap<String, String> hashMap;
    private OnRecyclerClickListner listner;

    public JarakAdapter(ArrayList<JarakHelper> arrayList, OnRecyclerClickListner listener) {
        this.arrayList = arrayList;
        this.listner = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nama, jarak;
        public MyViewHolder(View itemView) {
            super(itemView);
            Log.v("ViewHolder","in View Holder");
            nama = itemView.findViewById(R.id.NamaPos);
            jarak = itemView.findViewById(R.id.JarakPos);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onRecyclerViewItemClicked(getAdapterPosition(),view.getId());
                }
            });
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int   viewType) {
        Log.v("CreateViewHolder", "in onCreateViewHolder");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jarak_row,parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int  position) {
        Log.v("BindViewHolder", "in onBindViewHolder");
        JarakHelper jarakHelper = arrayList.get(position);
        holder.nama.setText(jarakHelper.getNamaPos());
        holder.jarak.setText(jarakHelper.getJarakPos() + " km");

        holder.nama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onRecyclerViewItemClicked(position,view.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size() ;
    }

}