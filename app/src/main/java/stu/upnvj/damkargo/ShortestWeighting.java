package stu.upnvj.damkargo;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.weighting.AbstractWeighting;
import com.graphhopper.util.EdgeIteratorState;

public class ShortestWeighting extends AbstractWeighting {
    public ShortestWeighting(FlagEncoder flagEncoder) {
        super(flagEncoder);
    }

    @Override
    public double getMinWeight(double currDistToGoal) {
        return currDistToGoal;
    }

    @Override
    public double calcWeight(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
        return edgeState.getDistance();
    }

    @Override
    public String getName() {
        return "my_custom_weighting";
    }
}
