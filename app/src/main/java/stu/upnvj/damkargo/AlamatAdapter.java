package stu.upnvj.damkargo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class AlamatAdapter extends RecyclerView.Adapter<AlamatAdapter.ViewHolder> {

    private ArrayList<AlamatHelper> arrayAlamat;
    private OnRecyclerClickListner listner;

    public AlamatAdapter(ArrayList<AlamatHelper> inputData, OnRecyclerClickListner listener) {
        this.arrayAlamat = inputData;
        this.listner = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView primary;
        public TextView secondary;

        public ViewHolder(View v) {
            super(v);
            primary = v.findViewById(R.id.primary);
            secondary = v.findViewById(R.id.secondary);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onRecyclerViewItemClicked(getAdapterPosition(),view.getId());
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alamat_row,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - mengambil elemen dari dataset (ArrayList) pada posisi tertentu
        // - mengeset isi view dengan elemen dari dataset tersebut
        AlamatHelper alamatHelper = arrayAlamat.get(position);
        holder.primary.setText(alamatHelper.getPrimary());
        holder.secondary.setText(alamatHelper.getSecondary());

        holder.primary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onRecyclerViewItemClicked(position,view.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayAlamat.size();
    }
}
